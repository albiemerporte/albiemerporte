![Screenshot 2024-02-06 8 59 41 PM](https://github.com/albiemer/albiemer/assets/36027987/2ae84bcd-96b6-4807-84e6-2636e9d65791)
<h1> Hi there, I am Albiemer Porte developer, as of now i am graduating student as Computer Science. 👋</h1><br><br><br>


## MY BOOKS
<a href="https://gitlab.com/albiemerporte/software-engineering-life-cycle-reference#software-engineering-life-cycle-reference">SOFTWARE ENGINEERING LIFE CYCLE</a>

<a href="https://gitlab.com/albiemerporte/research-study/-/tree/main">RESEARCH STUDY</a>

## MY TUTORIAL VLOGS

Link: https://youtu.be/543CgLMLFN0 <br>
Snippets: https://gitlab.com/albiemerporte/graph-with-vertical-slider.git
![thumbnail](https://github.com/albiemer/albiemer/assets/36027987/337932e6-e9f1-4c93-bc71-43132490a048)


Link: https://youtu.be/7v-FEg7DUEk <br>
Snippets: https://gitlab.com/albiemerporte/font-change-with-combo-box.git
![thumbnail](https://github.com/albiemer/albiemer/assets/36027987/a1909ee4-d165-40c6-be3a-a1b2d09a89b8)

Link: https://youtu.be/S5U_600k_b4 <br>
Snippets: https://gitlab.com/albiemerporte/graph-periodical-update.git
![thumbnail](https://github.com/albiemer/albiemer/assets/36027987/0274b2a1-9308-4493-b361-3227dc35ec7e)

Link: https://youtu.be/7hjiAAqZmTI <br>
Snippets: https://gitlab.com/albiemerporte/horizontal-slider-progress-bar-loader.git
![Screenshot 2024-02-07 9 50 58 AM](https://github.com/albiemer/albiemer/assets/36027987/6d722ce6-bf78-40bd-8bdb-dcd9c6ecfc8f)

Link: https://youtu.be/uC4ucrgQ0aE <br>
Snippets: https://gitlab.com/albiemerporte/check-task-progress.git
![Screenshot 2024-02-07 9 52 35 AM](https://github.com/albiemer/albiemer/assets/36027987/a574a2d5-fec2-4325-a8de-f79677918341)

Link: https://youtu.be/VfZFXY59MOc <br>
Snippets: https://gitlab.com/albiemerporte/dynamic-listbox-with-checkbox-entries.git
![Screenshot 2024-02-07 9 54 17 AM](https://github.com/albiemer/albiemer/assets/36027987/9e326111-1c6e-4652-88b4-f42da179c369)

Link: https://youtu.be/vV-HTJAppDE <br>
Snippets: https://gitlab.com/albiemerporte/populate-listbox-with-combobox-data.git
![Screenshot 2024-02-07 9 55 37 AM](https://github.com/albiemer/albiemer/assets/36027987/23bae969-8cf0-4d14-afab-dc5943e4b1ce)

Link: https://youtu.be/iYv4fDfAtj4 <br>
Snippets: https://gitlab.com/albiemerporte/python-qprint-ref.git
![Screenshot 2024-02-07 9 56 43 AM](https://github.com/albiemer/albiemer/assets/36027987/a81e332a-2b45-4737-b1db-b83e7881f890)

Link: https://youtu.be/Os_NKlaj5k4 <br>
Snippets: https://gitlab.com/albiemerporte/delete-row-checkedbox/-/tree/main
![Screenshot 2024-02-07 9 57 47 AM](https://github.com/albiemer/albiemer/assets/36027987/9b39aad7-9f39-42f4-b8fb-7583d2268a7c)

Link: https://youtu.be/A_s5D-k6evo <br>
Snippets: https://gitlab.com/albiemerporte/qlistwidgetref
![Screenshot 2024-02-07 10 05 28 AM](https://github.com/albiemer/albiemer/assets/36027987/a640ca17-c3e7-4340-871a-e7e024fc5a4a)

Link: https://youtu.be/X4-_i1QBK5I <br>
Snippets: https://gitlab.com/albiemerporte/current-index-selector.git
![Screenshot 2024-02-07 10 06 41 AM](https://github.com/albiemer/albiemer/assets/36027987/3b6053ca-8d1d-4df3-ac6a-fa0ece3741a3)

Link: https://youtu.be/r2CK52QdhU0 <br>
Snippets: No Snippets, Tutorial Only
![Screenshot 2024-02-07 10 08 50 AM](https://github.com/albiemer/albiemer/assets/36027987/c03864ec-8d84-4bfe-af39-3f115d24757e)

Link: https://youtu.be/tG0QBCDquIM <br>
Snippets: https://gitlab.com/albiemerporte/qtable-reference
![Screenshot 2024-02-07 10 10 02 AM](https://github.com/albiemer/albiemer/assets/36027987/5fc728b5-cea4-4587-9926-12e56d442f60)

Link: https://youtu.be/KOX_ZvSqG3w <br>
Snippets: https://gitlab.com/albiemerporte/listing-file-and-text-editor-using-load-ui-in-python3-programming.git
![Screenshot 2024-02-07 10 12 44 AM](https://github.com/albiemer/albiemer/assets/36027987/18842796-a835-4602-8f32-7594efad3837)

